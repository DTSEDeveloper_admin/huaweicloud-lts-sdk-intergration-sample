FROM openjdk:8u181-jdk-alpine
RUN mkdir -p /home/huawei/microservice
RUN echo "Asia/Shanghai" > /etc/timezone
COPY target/huaweicloud-LTS-SDK-intergration-sample-0.0.1-SNAPSHOT.jar /home/huawei/microservice/

ENTRYPOINT ["java","-jar","/home/huawei/microservice/huaweicloud-LTS-SDK-intergration-sample-0.0.1-SNAPSHOT.jar"]

EXPOSE 8080