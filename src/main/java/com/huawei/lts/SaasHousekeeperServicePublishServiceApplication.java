/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.huawei.**"})
public class SaasHousekeeperServicePublishServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SaasHousekeeperServicePublishServiceApplication.class, args);
    }

}