/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.service;

import com.huawei.lts.controller.request.*;
import com.huawei.lts.controller.response.ServiceSkuInfoRespVo;

/**
 * 功能描述 服务SKU功能类
 *
 * @author jWX1116205
 * @since 2022-02-15
 */
public interface IHouseKeeperServiceSkuService {
    /**
     * 根据skuid拿到服务详细描述
     *
     * @param getSkuDetailBySkuIdDto
     * @return {@link ServiceSkuInfoRespVo}
     */
    ServiceSkuInfoRespVo getSkuDetailBySkuId(GetSkuDetailBySkuIdDto getSkuDetailBySkuIdDto);

    /**
     * 创建服务规格
     *
     * @param createServiceSpecificationDto
     * @return {@link Long}
     */
    Long createHousekeeperServiceSpecification(CreateServiceSpecificationDto createServiceSpecificationDto);

    /**
     * 创建服务选集
     *
     * @param createServiceSelectionDto
     * @return {@link String}
     */
    String createHousekeeperServiceSelection(CreateServiceSelectionDto createServiceSelectionDto);

    /**
     * 删除服务规格
     *
     * @param deleteServiceSpecificationDto
     * @return {@link Long}
     */
    Long deleteHousekeeperServiceSpecification(DeleteSpecificationDto deleteServiceSpecificationDto);

    /**
     * 删除服务Sku
     *
     * @param deleteServiceSkuDto
     * @return {@link Long}
     */
    Long deleteHousekeeperServiceSkuBySkuId(DeleteServiceSkuDto deleteServiceSkuDto);

    /**
     * 更新服务Sku
     *
     * @param updateSeriviceSkuDto
     * @return {@link Long}
     */
    Long updateSeriviceSku(UpdateSeriviceSkuDto updateSeriviceSkuDto);
}