/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.service;

import com.huawei.lts.common.result.ListRes;
import com.huawei.lts.controller.request.*;
import com.huawei.lts.controller.response.HouseKeeperServiceListVo;
import com.huawei.lts.controller.response.HouseKeeperServiceVo;
import com.huawei.lts.controller.response.ServiceInfoRespVo;

/**
 * 功能描述 家政服务接口类
 *
 * @author jWX1116205
 * @since 2022-01-17
 */
public interface IHouseKeeperService {

    /**
     * 分页查询服务列表
     *
     * @param getServiceListReq
     * @return {@link ListRes<  HouseKeeperServiceListVo >}
     */
    ListRes<HouseKeeperServiceListVo> getHousekeeperServicePageByParam(GetServiceListDto getServiceListReq);

    /**
     * 服务预定首页查询
     *
     * @param getServiceListReq
     * @return {@link ListRes< HouseKeeperServiceListVo>}
     */
    ListRes<HouseKeeperServiceListVo> getHousekeeperServiceIndexPageByParam(GetServiceListDto getServiceListReq);

    /**
     * 根据服务id查询服务基本信息
     *
     * @param getServiceListReq
     * @return {@link HouseKeeperServiceVo}
     */
    HouseKeeperServiceVo queryHousekeeperServiceVoById(GetServiceDto getServiceListReq);

    /**
     * 根据服务id查询服务详细信息
     *
     * @param getServiceListReq
     * @return {@link ServiceInfoRespVo}
     */
    ServiceInfoRespVo getHousekeeperServiceVoById(GetServiceDto getServiceListReq);

    /**
     * 创建服务
     *
     * @param createServiceDto
     * @return {@link Long}
     */
    Long createHousekeeperService(CreateServiceDto createServiceDto);

    /**
     * 更新服务
     *
     * @param updateServiceDto
     * @return {@link Long}
     */
    Long updateHousekeeperService(UpdateServiceDto updateServiceDto);

    /**
     * 删除服务
     *
     * @param deleteServiceDto
     * @return {@link Long}
     */
    Long deleteHousekeeperService(DeleteServiceDto deleteServiceDto);
}