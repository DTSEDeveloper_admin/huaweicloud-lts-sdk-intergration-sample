/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.service;

import com.huawei.lts.controller.request.CreateTenantStyleCustomizationDto;
import com.huawei.lts.controller.request.UpdateTenantStyleCustomizationDto;
import com.huawei.lts.controller.response.TenantStyleCustomizationVo;

/**
 * 功能描述 服务主题配置
 *
 * @author jWX1116205
 * @since 2022-02-16
 */
public interface IStyleCustomizationService {
    /**
     * 创建服务配置或修改服务配置
     *
     * @param tenantStyleCustomizationDto
     * @return {@link String}
     */
    String createTenantStyleCustomization(CreateTenantStyleCustomizationDto tenantStyleCustomizationDto);

    /**
     * 修改租户主题配置
     *
     * @param updateTenantStyleCustomizationDto
     * @return {@link String}
     */

    String updateTenantStyleCustomization(UpdateTenantStyleCustomizationDto updateTenantStyleCustomizationDto);

    /**
     * 查询当前租户配置
     *
     * @return {@link TenantStyleCustomizationVo}
     */
    TenantStyleCustomizationVo getTenantStyleCustomizationVo(String tenantId);
}