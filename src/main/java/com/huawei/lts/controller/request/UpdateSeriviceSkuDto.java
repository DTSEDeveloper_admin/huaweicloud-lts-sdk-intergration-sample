/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Getter
@Setter
@ApiModel("编辑SKU")
public class UpdateSeriviceSkuDto {

    @NotNull(message = "skuId必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "skuId", required = true)
    private Long skuId;

    @NotEmpty
    @ApiModelProperty(value = "服务选项组合", required = true)
    private List<OptionProperty> optionProperties;

    @ApiModelProperty("价格")
    private BigDecimal price;

    @Data
    public static class OptionProperty {
        @ApiModelProperty("选项Id")
        private Long optionId;

        @ApiModelProperty("选项服务ID")
        private Long selectionId;

    }
}