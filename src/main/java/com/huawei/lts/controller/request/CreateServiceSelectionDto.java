/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Getter
@Setter
@ApiModel("创建服务选项对象")
public class CreateServiceSelectionDto {

    @NotNull(message = "服务ID必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "服务ID", required = true)
    private Long serviceId;

    @ApiModelProperty(value = "选项Id", required = true)
    private List<Long> optionIds;

    @NotNull(message = "服务选项价格必填")
    @Min(value = 0L, message = "最小值：0")
    @ApiModelProperty(value = "服务选项价格", required = true)
    private BigDecimal price;
}