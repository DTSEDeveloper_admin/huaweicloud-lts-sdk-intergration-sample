/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Getter
@Setter
@ApiModel("更新服务对象")
public class UpdateServiceDto {

    @NotNull(message = "serviceId必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "serviceId", required = true)
    private Long serviceId;

    @NotBlank(message = "服务名称必填")
    @Length(max = 255, message = "最大长度:255")
    @ApiModelProperty(value = "服务名称", required = true)
    private String serviceName;

    @Length(max = 500, message = "最大长度:500")
    @ApiModelProperty(value = "服务描述", required = true)
    private String serviceDesc;

    @Length(max = 500, message = "最大长度:500")
    @ApiModelProperty("图片地址")
    private String imgSrc;

    @NotNull(message = "服务展示价格必填")
    @Min(value = 0L, message = "最小值：0")
    @ApiModelProperty(value = "服务展示价格", required = true)
    private BigDecimal displayPrice;
}