/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import com.huawei.lts.common.constant.StyleFlagEnum;
import com.huawei.lts.common.validator.ActionTypeEnumValid;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel("创建租户配置对象")
public class CreateTenantStyleCustomizationDto {

    @NotNull(message = "主题标识为必填项，不得为空")
    @ActionTypeEnumValid(enumClass = StyleFlagEnum.class, message = "主题标识验证非法!有效值[1,2,3,4]")
    @ApiModelProperty(value = "主题标识(1,水瀑_waterfall;2,炭黑_charcoal;3,火红_firey_red;4,明黄_ILLUMINATING)", required = true,
            allowableValues = "1,2,3,4")
    private String styleFlag;
    @NotNull(message = "租户标识为必填项，不得为空")
    private String tenantId;

}