/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Getter
@Setter
@ApiModel("删除服务规格对象")
public class DeleteSpecificationDto {

    @NotNull(message = "规格id必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "规格id", required = true)
    private Long specId;
}