/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import com.huawei.lts.common.request.PageRequest;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

/**
 * @author jWX1116205
 * @since 2022-01-04
 */
@Setter
@Getter
@ApiModel("查询服务列表对象")
public class GetServiceListDto extends PageRequest {

    @ApiModelProperty("服务名称")
    @Length(max = 120)
    private String serviceName;

    @ApiModelProperty(value = "服务状态(1,申请中;2,运行中;3,终止;4,升级改造)", allowableValues = "1,2,3,4", hidden = true)
    @Length(max = 1)
    private String servieStatus;

}