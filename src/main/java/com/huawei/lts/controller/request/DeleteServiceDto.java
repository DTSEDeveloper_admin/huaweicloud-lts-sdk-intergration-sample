/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author jWX1116205
 * @since 2022-02-10
 */
@Getter
@Setter
@ApiModel("服务删除对象")
public class DeleteServiceDto {

    @NotNull(message = "服务ID必填")
    @Min(value = 1L, message = "最小值：1")
    @ApiModelProperty(value = "serviceId", required = true)
    private Long serviceId;

}