/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author jWX1116205
 * @since 2022-02-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询服务列表响应vo")
public class HouseKeeperServiceListVo {

    @ApiModelProperty("服务ID")
    private Long id;

    @ApiModelProperty("服务名称")
    private String serviceName;

    @ApiModelProperty("服务描述")
    private String serviceDesc;

    @ApiModelProperty("创建人")
    private String createdBy;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新人")
    private String updatedBy;

    @ApiModelProperty("更新时间")
    private Date updatedTime;

    @ApiModelProperty("服务状态")
    private String servieStatus;

    @ApiModelProperty("删除标志")
    private String deleteFlag;

    @ApiModelProperty("乐观锁")
    private String revision;

    @ApiModelProperty("服务展示价格")
    private BigDecimal displayPrice;

    @ApiModelProperty("服务规格名称")
    private String specificationName;

    @ApiModelProperty("图片地址")
    private String imgSrc;

}