/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @author jWX1116205
 * @since 2022-01-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("根据skuId拿到服务详细描述响应vo")
public class GetSkuDetailBySkuIdVo {

    @ApiModelProperty("服务ID")
    private Long serviceId;

    @ApiModelProperty("服务名称")
    private String serviceName;

    @ApiModelProperty("服务描述")
    private String serviceDesc;

    @ApiModelProperty("规格ID")
    private Long specificationId;

    @ApiModelProperty("选项ID")
    private Long optionId;

    @ApiModelProperty("选项服务ID")
    private Long selectionId;

    @ApiModelProperty("选项名称")
    private String optionName;

    @ApiModelProperty("规格名称")
    private String specName;

    @ApiModelProperty("SKU_ID")
    private Long skuId;

    @ApiModelProperty("选项服务价格")
    private BigDecimal price;

    @ApiModelProperty("服务展示价格")
    private BigDecimal displayPrice;

    @ApiModelProperty("图片地址")
    private String imgSrc;
}