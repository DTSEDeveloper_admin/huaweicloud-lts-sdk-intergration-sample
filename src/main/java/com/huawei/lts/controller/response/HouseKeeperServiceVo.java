/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author jWX1116205
 * @since 2022-01-17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询服务响应vo")
public class HouseKeeperServiceVo {
    @ApiModelProperty("服务ID")
    private Long serviceId;

    @ApiModelProperty("服务名称")
    private String serviceName;

    @ApiModelProperty("服务描述")
    private String serviceDesc;

    @ApiModelProperty("创建人")
    private String createdBy;

    @ApiModelProperty("创建时间")
    private Date createTime;

    @ApiModelProperty("更新人")
    private String updatedBy;

    @ApiModelProperty("更新时间")
    private Date updatedTime;

    @ApiModelProperty("服务状态")
    private String servieStatus;

    @ApiModelProperty("规格ID")
    private Long specificationId;

    @ApiModelProperty("规格名称")
    private String specificationName;

    @ApiModelProperty("选项ID")
    private Long optionId;

    @ApiModelProperty("选项服务ID")
    private Long selectionId;

    @ApiModelProperty("选项名称")
    private String optionName;

    @ApiModelProperty("规格名称")
    private String specName;

    @ApiModelProperty("SKU_ID")
    private Long skuId;

    @ApiModelProperty("选项服务价格")
    private BigDecimal price;

    @ApiModelProperty("服务展示价格")
    private BigDecimal displayPrice;

    @ApiModelProperty("图片地址")
    private String imgSrc;
}