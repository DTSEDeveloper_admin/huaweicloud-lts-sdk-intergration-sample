/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("查询租户配置响应vo")
public class TenantStyleCustomizationVo {
    @ApiModelProperty("配置ID")
    private Long configId;

    @ApiModelProperty(value = "主题标识(1,水瀑_waterfall;2,炭黑_charcoal;3,火红_firey_red;4,明黄_ILLUMINATING)", required = false,
            allowableValues = "1,2,3,4")
    private Long styleFlag;

    @ApiModelProperty("店名")
    private String storeName;

    @ApiModelProperty("租户标识")
    private String tenantId;

}