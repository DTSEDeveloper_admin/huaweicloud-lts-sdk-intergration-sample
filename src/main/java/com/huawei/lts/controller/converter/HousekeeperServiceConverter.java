/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller.converter;

import com.huawei.lts.controller.request.CreateServiceDto;
import com.huawei.lts.controller.request.GetServiceDto;
import com.huawei.lts.controller.request.UpdateServiceDto;
import com.huawei.lts.dao.entity.HousekeeperService;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * mapstruct转换器
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper(componentModel = "spring")
public interface HousekeeperServiceConverter {
    /**
     * 对象映射转换实例
     */
    HousekeeperServiceConverter INSTANCE = Mappers.getMapper(HousekeeperServiceConverter.class);

    /**
     * 前端数据转转成数据库实例
     *
     * @param housekeeperServiceDto
     * @return HousekeeperService
     */
    HousekeeperService createHousekeeperServiceByDto(GetServiceDto getServiceDto);

    HousekeeperService createHousekeeperServiceByDto(CreateServiceDto housekeeperServiceDto);

    HousekeeperService createHousekeeperServiceByDto(UpdateServiceDto updateServiceDto);
}