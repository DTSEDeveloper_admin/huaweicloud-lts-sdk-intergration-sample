/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller;

import com.huawei.lts.common.result.Result;
import com.huawei.lts.controller.request.CreateTenantStyleCustomizationDto;
import com.huawei.lts.controller.request.UpdateTenantStyleCustomizationDto;
import com.huawei.lts.controller.response.TenantStyleCustomizationVo;
import com.huawei.lts.service.IStyleCustomizationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 功能描述 租户主题配置
 *
 * @author jWX1116205
 * @since 2022-01-17
 */
@Api(tags = "租户主题配置")
@RestController
@Validated
@RequestMapping("/styleCustomization")
public class TenantStyleCustomizationController {
    @Autowired
    private IStyleCustomizationService styleCustomizationService;

    @ApiOperation("租户添加主题或更新配置")
    @PostMapping("/createTenantStyleCustomization")
    public Result<String> createTenantStyleCustomization(
            @Valid @RequestBody CreateTenantStyleCustomizationDto tenantStyleCustomizationDto,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
                styleCustomizationService.createTenantStyleCustomization(tenantStyleCustomizationDto));
    }

    @ApiOperation("租户修改主题配置")
    @PostMapping("/updateTenantStyleCustomization")
    public Result<String> updateTenantStyleCustomization(
            @Valid @RequestBody UpdateTenantStyleCustomizationDto updateTenantStyleCustomizationDto,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
                styleCustomizationService.updateTenantStyleCustomization(updateTenantStyleCustomizationDto));
    }

    @ApiOperation("租户查询主题配置")
    @PostMapping("/getTenantStyleCustomization")
    public Result<TenantStyleCustomizationVo> getTenantStyleCustomization(
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(styleCustomizationService.getTenantStyleCustomizationVo(tenantDomain));
    }
}