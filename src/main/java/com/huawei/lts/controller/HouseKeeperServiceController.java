/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.controller;

import com.huawei.lts.common.log.aop.LogMethod;
import com.huawei.lts.common.result.ListRes;
import com.huawei.lts.common.result.Result;
import com.huawei.lts.controller.request.*;
import com.huawei.lts.controller.response.HouseKeeperServiceListVo;
import com.huawei.lts.controller.response.HouseKeeperServiceVo;
import com.huawei.lts.controller.response.ServiceInfoRespVo;
import com.huawei.lts.controller.response.ServiceSkuInfoRespVo;
import com.huawei.lts.service.IHouseKeeperService;
import com.huawei.lts.service.IHouseKeeperServiceSkuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 功能描述 家政服务接口
 *
 * @author jWX1116205
 * @since 2022-01-17
 */
@Api(tags = "家政服务接口") // 作用在模块API类上，对API模块进行说明
@RestController
@Validated
@RequestMapping("/housekeeper")
public class HouseKeeperServiceController {

    @Autowired
    IHouseKeeperService housekeeperService;

    @Autowired
    IHouseKeeperServiceSkuService houseKeeperServiceSkuService;

    @LogMethod(title = "服务添加", operatorType = "add", content = "服务添加")
    @ApiOperation("服务添加")
    @PostMapping("/createService")
    public Result<Long> createService(@Valid @RequestBody CreateServiceDto createServiceDto,
                                      @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.createHousekeeperService(createServiceDto));
    }

    @LogMethod(title = "服务修改", operatorType = "edit", content = "服务修改")
    @ApiOperation("服务修改")
    @PostMapping("/updateService")
    public Result<Long> updateService(@Valid @RequestBody UpdateServiceDto updateServiceDto,
                                      @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.updateHousekeeperService(updateServiceDto));
    }

    @LogMethod(title = "服务删除", operatorType = "delete", content = "服务删除")
    @ApiOperation("服务删除")
    @DeleteMapping("/deleteService")
    public Result<Long> deleteService(@Valid @RequestBody DeleteServiceDto deleteServiceDto,
                                      @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.deleteHousekeeperService(deleteServiceDto));
    }

    @LogMethod(title = "服务添加规格", operatorType = "add", content = "服务添加规格")
    @ApiOperation("服务添加规格")
    @PostMapping("/createServiceSpecification")
    public Result<Long> createServiceSpecification(
            @Valid @RequestBody CreateServiceSpecificationDto createServiceSpecificationDto,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
                houseKeeperServiceSkuService.createHousekeeperServiceSpecification(createServiceSpecificationDto));
    }

    @LogMethod(title = "添加服务选项", operatorType = "add", content = "添加服务选项")
    @ApiOperation("添加服务选项")
    @PostMapping("/createServiceSelection")
    public Result<String> createServiceSelection(
            @Valid @RequestBody CreateServiceSelectionDto createServiceSelectionDto,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
                houseKeeperServiceSkuService.createHousekeeperServiceSelection(createServiceSelectionDto));
    }

    @LogMethod(title = "编辑服务选项", operatorType = "edit", content = "编辑服务选项")
    @ApiOperation("编辑服务选项")
    @PostMapping("/updateServiceSelection")
    public Result<Long> updateServiceSelection(@Valid @RequestBody UpdateSeriviceSkuDto updateSeriviceSkuDto,
                                               @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(houseKeeperServiceSkuService.updateSeriviceSku(updateSeriviceSkuDto));
    }

    @LogMethod(title = "删除服务规格", operatorType = "delete", content = "删除服务规格")
    @ApiOperation("删除服务规格")
    @DeleteMapping("/deleteSpecification")
    public Result<Long> deleteSpecification(@Valid @RequestBody DeleteSpecificationDto deleteSpecificationDto,
                                            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
                houseKeeperServiceSkuService.deleteHousekeeperServiceSpecification(deleteSpecificationDto));
    }

    @LogMethod(title = "删除SKU", operatorType = "delete", content = "删除SKU")
    @ApiOperation("删除SKU")
    @DeleteMapping("/deleteServiceSkuBySkuId")
    public Result<Long> deleteHousekeeperServiceSkuBySkuId(@Valid @RequestBody DeleteServiceSkuDto deleteServiceSkuDto,
                                                           @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(
                houseKeeperServiceSkuService.deleteHousekeeperServiceSkuBySkuId(deleteServiceSkuDto));
    }

    @LogMethod(title = "服务明细查询", operatorType = "query", content = "服务明细查询")
    @ApiOperation("服务明细查询")
    @PostMapping("/queryServiceById")
    public Result<HouseKeeperServiceVo> getHousekeeperServiceVoById(@Valid @RequestBody GetServiceDto getServiceListReq,
                                                                    @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.queryHousekeeperServiceVoById(getServiceListReq));
    }

    @LogMethod(title = "单个服务查询", operatorType = "query", content = "单个服务查询")
    @ApiOperation("单个服务查询")
    @PostMapping("/getServiceById")
    public Result<ServiceInfoRespVo> getServiceById(@Valid @RequestBody GetServiceDto getServiceListReq,
                                                    @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.getHousekeeperServiceVoById(getServiceListReq));
    }

    @LogMethod(title = "服务查询", operatorType = "query", content = "服务管理列表查询")
    @ApiOperation("服务管理列表查询")
    @PostMapping("/getServiceList")
    public Result<ListRes<HouseKeeperServiceListVo>> getServiceList(
            @Valid @RequestBody GetServiceListDto getServiceListReq,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.getHousekeeperServicePageByParam(getServiceListReq));
    }

    @LogMethod(title = "服务查询", operatorType = "query", content = "服务首页查询")
    @ApiOperation("服务首页查询")
    @PostMapping("/getServiceIndexList")
    public Result<ListRes<HouseKeeperServiceListVo>> getServiceIndexList(
            @Valid @RequestBody GetServiceListDto getServiceListReq,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(housekeeperService.getHousekeeperServiceIndexPageByParam(getServiceListReq));
    }

    @LogMethod(title = "服务SKU查询", operatorType = "query", content = "根据skuid拿到服务详细描述")
    @ApiOperation("根据skuid拿到服务详细描述")
    @PostMapping("/getSkuDetailBySkuId")
    public Result<ServiceSkuInfoRespVo> getSkuDetailBySkuId(
            @Valid @RequestBody GetSkuDetailBySkuIdDto getSkuDetailBySkuIdDto,
            @RequestHeader(defaultValue = "housekeeping", name = "tenantDomain") String tenantDomain) {
        return Result.createResult(houseKeeperServiceSkuService.getSkuDetailBySkuId(getSkuDetailBySkuIdDto));
    }
}