/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huawei.lts.dao.entity.Customization;
import org.apache.ibatis.annotations.Mapper;

/**
 * 租户配置表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface CustomizationMapper extends BaseMapper<Customization> {

}
