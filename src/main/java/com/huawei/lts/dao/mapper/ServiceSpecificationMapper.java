/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huawei.lts.dao.entity.ServiceSpecification;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * 规格表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface ServiceSpecificationMapper extends BaseMapper<ServiceSpecification> {
    int deleteSpecificationByIds(List<Long> idList, String updatedBy);

    int getCountByServiceSpecificationName(Map<String, Object> params);
}