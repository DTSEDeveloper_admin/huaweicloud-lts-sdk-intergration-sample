/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huawei.lts.dao.entity.ServiceOption;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 规格选项表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface ServiceOptionMapper extends BaseMapper<ServiceOption> {
    int deleteBySpecificationIds(List<Long> specIds, String updatedBy);

    int insertBatch(List<ServiceOption> records);
}