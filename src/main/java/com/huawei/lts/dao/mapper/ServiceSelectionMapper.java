/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huawei.lts.controller.response.GetServiceSelectionByServiceIdVo;
import com.huawei.lts.dao.entity.ServiceSelection;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 选集表Mapper接口
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Mapper
public interface ServiceSelectionMapper extends BaseMapper<ServiceSelection> {
    int deleteByOptionIds(List<Long> optionIds, String updatedBy);

    int deleteBySkuIds(List<Long> skuIds, String updatedBy);

    int insertBatch(List<ServiceSelection> records);

    List<GetServiceSelectionByServiceIdVo> getServiceSelectionVoListByServiceId(Long serviceId);
}