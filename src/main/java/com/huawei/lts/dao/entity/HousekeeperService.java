/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.dao.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * 服务主表
 *
 * @author jwx1116205
 * @since 2022-03-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_housekeeper_service")
public class HousekeeperService extends BaseEntity {
    /**
     * 服务名称
     */
    @TableField("service_name")
    private String serviceName;

    /**
     * 服务描述
     */
    @TableField("service_desc")
    private String serviceDesc;

    /**
     * 图片地址
     */
    @TableField("img_src")
    private String imgSrc;

    /**
     * 服务状态
     */
    @TableField("servie_status")
    private String servieStatus;

    /**
     * 删除标志
     */
    @TableField("delete_flag")
    private String deleteFlag;

    /**
     * 乐观锁
     */
    @TableField("revision")
    private String revision;

    /**
     * 服务展示价格
     */
    @TableField("display_price")
    private BigDecimal displayPrice;

}