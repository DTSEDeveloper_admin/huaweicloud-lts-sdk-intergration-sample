/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2022. All rights reserved.
 */

package com.huawei.lts.common.constant;

import lombok.Getter;

/**
 * 状态码枚举类
 *
 * @since 2021-11-30
 */
@Getter
public enum ErrorCode implements BaseCode {

    SERVICE_NAME_REPEATED(220000, "服务名重复!"),

    SERVICE_NOT_EXIST(220001, "服务不存在!"),

    SPECIFICATION_REPEATED(220007, "服务规格已存在!"),

    SPECIFICATION_NOT_EXIST(2200012, "服务规格不存在!"),

    SKU_NOT_EXIST(220008, "该skuId没有对应的数据!"),

    SELECZTION_REPEATED(220009, "服务选集重复!"),

    PRIMARY_KEY_NOT_NULL(220010, "主键不能为空!"),

    BUSINESS_DATA_ERROR(220011, "数据逻辑非法!"),

    BUSINESS(220002, "业务异常!"),

    UNKNOWN(220003, "未知异常!"),

    PARAM_INVALID(220004, "参数非法!"),

    RUN_TIME_EXCEPTION(220006, "运行期异常!"),

    NULL_POINTER_EXCEPTION(220005, "空指针异常!");

    private int code;

    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}