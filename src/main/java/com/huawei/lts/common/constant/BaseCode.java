/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.lts.common.constant;

/**
 * 状态码超类
 */
public interface BaseCode {

    int getCode();

    String getMessage();
}