/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.lts.common.constant;

import com.baomidou.mybatisplus.core.toolkit.StringPool;

/**
 * 通用常量类
 */
public interface BaseConstant {
    // cors常量
    interface Header {
        static final String HEADER_ORIGIN = "Access-Control-Allow-Origin";

        static final String HEADER_METHODS = "Access-Control-Allow-Methods";

        // 请求头Methods的值
        static final String METHODS_VALUES = "PUT,DELETE,POST,GET,OPTIONS";

        static final String HEADER_AGE = "Access-Control-Max-Age";

        static final String HEADER_HEADERS = "Access-Control-Allow-Headers";

        static final String HEADERS_TYPE = "Content-Type";
    }

    // 符号常量
    interface Symbol extends StringPool {

    }
}
