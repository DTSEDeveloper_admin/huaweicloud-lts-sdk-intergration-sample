/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2022-2022. All rights reserved.
 */

package com.huawei.lts.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * 功能描述 用于校验枚举值是否被包含在数据内
 *
 * @author jWX1116205
 * @since 2022-01-05
 */
public class ActionTypeEnumValidator implements ConstraintValidator<ActionTypeEnumValid, String> {
    private Class<? extends Enum> enumClass;

    @Override
    public void initialize(ActionTypeEnumValid actionTypeEnumValid) {
        enumClass = actionTypeEnumValid.enumClass();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || "".equals(value)) {
            return true;
        }

        EnumValidate[] enums = (EnumValidate[]) enumClass.getEnumConstants();
        if (enums == null || enums.length == 0) {
            return false;
        }

        return enums[0].existValidate(value);
    }
}