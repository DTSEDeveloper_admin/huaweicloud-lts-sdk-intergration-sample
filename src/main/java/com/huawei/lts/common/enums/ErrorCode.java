/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.lts.common.enums;

import com.huawei.lts.common.constant.BaseCode;
import lombok.Getter;

/**
 * 状态码枚举类
 *
 * @since 2021-11-30
 */
@Getter
public enum ErrorCode implements BaseCode {
    FORBIDDEN(110001, "没有权限"),

    BUSINESS(110002, "业务异常"),

    UNKNOWN(110003, "未知异常"),

    PARAM_INVALID(110004, "参数非法"),

    USERNAME_OR_PASSWORD_ERROR(110005, "用户名或者密码错误"),

    TOKEN_EXPIRED(110006, "token过期"),

    TOKEN_ERROR(110007, "token错误"),

    IS_EMPTY(110008, "参数为空"),

    JSON_EXECPTION(110009, "json异常"),

    NULL_POINTTER(110010, "空指针异常");

    private int code;

    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}