/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.huawei.lts.common.exception;

import com.huawei.lts.common.constant.BaseCode;
import lombok.Getter;
import lombok.Setter;

/**
 * 业务异常类
 */
@Setter
@Getter
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -4989791274483505337L;

    private int code;

    public BusinessException(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }

    public BusinessException(int code, String message) {
        this(code, message, null);
    }

    public BusinessException(BaseCode code, Throwable cause) {
        this(code.getCode(), code.getMessage(), cause);
    }

    public BusinessException(BaseCode code) {
        this(code, null);
    }
}