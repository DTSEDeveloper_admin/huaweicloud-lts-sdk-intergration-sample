package com.huawei.lts.common.config;

import com.huawei.lts.common.log.LogLtsSchedule;
import com.huawei.lts.common.log.lts.ClientFactBean;
import com.huawei.lts.common.log.lts.LogItemsUtil;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(prefix = "log.lts", name = "enabled", havingValue = "true")
public class LtsLogConfig {
    @Bean
    public ClientFactBean clientFactBean() {
        return new ClientFactBean();
    }

    @Bean
    public LogLtsSchedule logLtsSchedule() {
        return new LogLtsSchedule();
    }

    @Bean
    public LogItemsUtil logItemsUtil() {
        return new LogItemsUtil();
    }
}
