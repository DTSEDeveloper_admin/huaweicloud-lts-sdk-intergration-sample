package com.huawei.lts.common.config;

import com.huawei.lts.common.log.aop.LogMethodAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AopConfig {
    @Bean
    public LogMethodAspect logMethodAspect() {
        return new LogMethodAspect();
    }
}
