## **项目介绍**

云日志服务（Log Tank Service，简称LTS），用于收集应用、云服务的日志数据，帮助企业快速搭建企业日志分析系统。本项目基于LTS Java SDK能力，结合业务使用场景，做了进一步封装，按照时间维度或者日志量维度自定义日志上报策略，帮助企业开发者快速搭建企业级日志分析系统。

### **参数指南**

`accessKey: 访问秘钥(通常为20位)`

`secretKey: 秘钥(通常为40位)`

`projectId: 项目Id`

`region:所属区域`

`groupId: 日志组Id`

`streamId:日志流Id`

`batchSize: 日志打包发送的最低日志数量可自定义`

`queueMaxSize: 日志容量`

### **技术选型**

| 技术                  | 说明                | 官网                                          |
| -------------------- | -------------------| --------------------------------------------  |
| SpringBoot         | 容器+MVC框架           | https://spring.io/projects/spring-boot        |
| Mysql           | 云数据库RDS         | https://support.huaweicloud.com/rds/index.html        |
| Lombok               | 简化对象封装工具             | https://github.com/rzwitserloot/lombok           |
| Swagger-UI       | 文档生成工具        | https://github.com/swagger-api/swagger-ui    |

### **开发环境**

| 工具          | 版本号 | 下载                                                           |
| ------------- | ------ | ------------------------------------------------------------ |
| JDK           | 1.8    | https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html |

### **项目验证**

[swagger访问地址](http://localhost:8080/doc.html#)

### **项目分支说明**

本项目包含分支：master主干、LTS-sdk-dev分支, 其中dev分支为master分支代码结构的优化

### **入门指南**

https://support.huaweicloud.com/usermanual-lts/lts_03_1003.html